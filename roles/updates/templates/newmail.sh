#!/bin/bash

#export REPLYTO="ansible@example.com"

EMAILS="Changemetoarelevantemailoremails"

TODAY=$(date +%F)

MBODY="Sending a report on today's updates. nd i can also be made more relevant if you want me to be"

DIR=roles/updates/files/

cd "$DIR"

for TEXTFILE in *
do
	ATTACHMENTS="$ATTACHMENTS -a $TEXTFILE"
done

echo $MBODY | mutt -s "Topic of the mail $TODAY" $ATTACHMENTS  -- $EMAILS